import React, {Component} from 'react';
import '../scss/components/Filter.scss';

export default class Filter extends Component {
    toggleFilters = () => {
        let activeClass = 'is-active';

        this.refs.filters.classList.toggle(activeClass);
        this.refs.openFilter.classList.toggle(activeClass);
    };
    sort = sort => {
        let activeClass = 'is-active';

        this.refs.filters.querySelectorAll('button').forEach((btn, index) => {
            btn.classList.remove(activeClass);
            if (index === sort) btn.classList.add(activeClass);
        });
        this.props.filter(sort);
        this.toggleFilters();
    };
    render() {
        return(
            <div className="box-filters">
                <button ref="openFilter" className="open-filters" onClick={this.toggleFilters}>
                    <i className="icon-filter"/>
                </button>
                <div ref="filters" className="filters">
                    <span>
                        Filter:
                    </span>
                    <button className="is-active" onClick={this.sort.bind(this, 0)}>
                        All
                    </button>
                    <button onClick={this.sort.bind(this, 1)}>
                        In progress
                    </button>
                    <button onClick={this.sort.bind(this, 2)}>
                        Completed
                    </button>
                    <button className="close-btn" onClick={this.toggleFilters}>
                        Close
                    </button>
                </div>
            </div>
        )
    }
}