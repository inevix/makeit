import React, {Component} from 'react';
import '../scss/components/Nav.scss';

export default class Nav extends Component {
    toggleNav = () => {
        let activeClass = 'is-active';

        this.refs.nav.classList.toggle(activeClass);
        this.refs.openNav.classList.toggle(activeClass);
    };
    action = action => {
        this.props.action(action);
        this.toggleNav();
    };
    render() {
        return(
            <div className="app-nav">
                <button ref="openNav" className="open-nav" onClick={this.toggleNav}>
                    <i className="icon-nav"/>
                </button>
                <div ref="nav" className="box-nav">
                    <span>
                        Actions:
                    </span>
                    <button onClick={this.action.bind(this, 0)}>
                        Complete all
                    </button>
                    <button onClick={this.action.bind(this, 1)}>
                        Uncomplete all
                    </button>
                    <button onClick={this.action.bind(this, 2)}>
                        Remove all
                    </button>
                    <button className="close-btn" onClick={this.toggleNav}>
                        Close
                    </button>
                </div>
            </div>
        )
    }
}