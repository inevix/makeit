import React, {Component} from 'react';
import Nav from './Nav';
import Filter from './Filter';
import Info from './Info';
import '../scss/components/Header.scss';
import logo from '../img/logo.svg';

export default class Header extends Component {
    action = action => this.props.action(action);
    render() {
        let tasks = this.props.tasks,
            hasTasks = tasks.length >= 1,
            nav = hasTasks ? <Nav action={this.action}/> : '',
            filter = '';

        if (hasTasks) {
            let tempComplete = [],
                tempInProgress = [];

            tasks.forEach(task => task.complete ? tempComplete.push(task) : tempInProgress.push(task));
            if (tempComplete.length >= 1 && tempInProgress.length >= 1) {
                filter = <Filter filter={this.props.filter}/>
            }
        }

        return(
            <header className="header">
                <div className="box-logo">
                    <div className="logo">
                        <img src={logo} alt="MakeIt"/>
                    </div>
                </div>
                <Info/>
                {filter}
                {nav}
            </header>
        )
    }
}