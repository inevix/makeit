import React, {Component} from 'react';
import '../scss/components/Task.scss';

export default class Task extends Component {
    constructor(tasks) {
        super();

        this.state = {
            complete: tasks.children.complete,
            edit: false,
            inputVal: tasks.children.task,
            collapsed: true
        };
        this.handleChange = this.handleChange.bind(this);
    }
    removeTask = () => this.props.remove(this.props.index);
    changeStatus = () => {
        let status = !this.state.complete;

        this.setState({
            complete: status
        });
        this.props.status(this.props.index, status);
    };
    toggleEditState = () => {
        let edit = !this.state.edit,
            newVal = this.state.inputVal;

        this.setState({
            edit: edit
        });
        if (!edit && newVal.length >= 1) this.props.update(newVal, this.props.index);
    };
    handleChange = event => {
        this.setState({
            inputVal: event.target.value
        });
        if (event.keyCode === 13) this.toggleEditState();
    };
    toggleText = () => {
        this.setState({
            collapsed: !this.state.collapsed
        })
    };
    textTask = text => {
        let maxLength = window.matchMedia('(max-width: 767px)').matches ? 50 : 200,
            collapsed = this.state.collapsed,
            linkLabel = collapsed ? 'Show' : 'Less',
            link = '',
            ellipsis = '';

        if (text.length > maxLength) {
            link = <span className="toggle-text" onClick={this.toggleText}>{linkLabel}</span>;

            if (collapsed) {
                ellipsis = <span className="clamp">...</span>;
                text = text.slice(0, maxLength);
            }
        }

        return(
            <div><span className="label" onClick={this.toggleEditState}>{text}</span>{ellipsis}{link}</div>
        )
    };
    render() {
        let classStatus = this.props.children.complete ? ' is-checked' : '',
            classEdit = this.state.edit ? ' is-edit' : '',
            classHide = !this.props.children.visible ? ' is-hide' : '',
            itemClass = classStatus+classEdit+classHide,
            textTask = this.props.children.task,
            textView = this.textTask(textTask);

        if (this.state.edit) {
            textView = <input defaultValue={textTask} onBlur={this.toggleEditState} onKeyUp={this.handleChange} onChange={this.handleChange} autoFocus/>;
        }

        return(
            <li className={itemClass}>
                <label onClick={this.changeStatus}>
                    <ins/>
                </label>
                {textView}
                <button className="remove-task" onClick={this.removeTask}>
                    <i className="icon-trash"/>
                </button>
            </li>
        )
    }
}