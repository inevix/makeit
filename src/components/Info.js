import React, {Component} from 'react';
import '../scss/components/Info.scss';

export default class Info extends Component {
    toggleInfo = () => {
        let activeClass = 'is-active';

        this.refs.info.classList.toggle(activeClass);
        this.refs.openInfo.classList.toggle(activeClass);
    };
    render() {
        return(
            <div className="app-info">
                <button ref="openInfo" className="open-info" onClick={this.toggleInfo}>
                    <i className="icon-info"/>
                </button>
                <div ref="info" className="box-info">
                    <div className="info">
                        <div className="author">
                            <span>
                                Author:
                            </span>
                            <a href="https://inevix.biz/" target="_blank">
                                inevix
                            </a>
                        </div>
                        <div className="version">
                            <span>
                                Version:
                            </span>
                            <span>
                                2.1.0
                            </span>
                        </div>
                    </div>
                    <button className="close-btn" onClick={this.toggleInfo}>
                        Close
                    </button>
                </div>
            </div>
        )
    }
}