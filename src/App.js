import React, {Component} from 'react';
import Task from './components/Task';
import './scss/App.scss';
import Header from "./components/Header";

export default class App extends Component {
    constructor() {
        super();

        let tasksArray = localStorage.getItem('tasks');

        if (tasksArray === undefined || tasksArray == null) {
            tasksArray = [];
            localStorage.setItem('tasks', '[]');
        }
        if (tasksArray.length >= 1) {
            tasksArray = JSON.parse(tasksArray);
            tasksArray.forEach(task => {
                task.visible = true
            });
            localStorage.setItem('tasks', JSON.stringify(tasksArray));
        } else {
            tasksArray = [];
        }

        this.state = {
            tasksArray: tasksArray,
            value: ''
        };
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange = event => {
        let target = event.target,
            value = target.value;

        this.setState({
            value: value
        });
        if (event.keyCode === 13) this.add();
        value.length >= 1 ? target.classList.remove('has-error') : target.classList.add('has-error');
    };
    blur = () => this.refs.addInput.classList.remove('has-error');
    add = () => {
        let input = this.refs.addInput,
            inputValue = input.value,
            tasksArray = localStorage.getItem('tasks');

        if (inputValue.length >= 1) {
            if (tasksArray === undefined || tasksArray == null) localStorage.setItem('tasks', '[]');
            tasksArray = tasksArray.length >= 1 ? JSON.parse(tasksArray) : [];
            tasksArray.push({
                task: inputValue,
                complete: false,
                visible: true
            });

            this.setArray(tasksArray);
            this.setState({
                value: ''
            });
            input.classList.remove('has-error');
        } else {
            input.classList.add('has-error');
        }
    };
    remove = index => {
        let tasksArray = this.state.tasksArray;

        tasksArray.splice(index, 1);
        this.setArray(tasksArray);
    };
    status = (index, status) => {
        let tasksArray = this.state.tasksArray;

        tasksArray[index].complete = status;
        this.setArray(tasksArray);
    };
    update = (task, index) => {
        let tasksArray = this.state.tasksArray;

        tasksArray[index].task = task;
        this.setArray(tasksArray);
    };
    filter = sortBy => {
        let tasksArray = this.state.tasksArray,
            temp = [];

        tasksArray.forEach(task => {
            task.visible = true;
            if (sortBy === 1 && task.complete === true) {
                task.visible = false;
            } else if (sortBy === 2 && task.complete === false) {
                task.visible = false;
            }
            temp.push(task);
        });
        this.setArray(temp);
        temp = [];
    };
    action = action => {
        let actionComplete = action === 0,
            actionUncomplete = action === 1,
            actionRemove = action === 2,
            tasksArray = this.state.tasksArray;

        if (actionComplete) {
            tasksArray.forEach(task => {
                task.complete = true;
            });
            this.setArray(tasksArray);
        } else if (actionUncomplete) {
            tasksArray.forEach(task => {
                task.complete = false;
            });
            this.setArray(tasksArray);
        } else if (actionRemove) {
            this.setArray([]);
        }
    };
    tasksList = (task, index) => {
        return(
            <Task key={index} index={index} remove={this.remove} status={this.status} update={this.update} visible={task.visible}>
                {task}
            </Task>
        )
    };
    setArray = array => {
        this.setState({
            tasksArray: array
        });
        localStorage.setItem('tasks', JSON.stringify(array));
    };
    render() {
        return (
            <div className="app-container">
                <Header filter={this.filter} action={this.action} tasks={this.state.tasksArray}/>
                <div className="user-panel">
                    <div className="add-task">
                        <input type="text"
                               ref="addInput"
                               onKeyUp={this.handleChange}
                               onChange={this.handleChange}
                               onBlur={this.blur}
                               value={this.state.value}
                               placeholder="What to do?"/>
                        <button ref="addBtn" onClick={this.add}>
                            <i className="icon-arrow"/>
                        </button>
                    </div>
                </div>
                <ul className="tasks-list">
                    {this.state.tasksArray.map(this.tasksList)}
                </ul>
            </div>
        );
    }
}